"""
    Flask App for testing Elastic Beanstalk and .ebextension
"""

from flask import Flask, request, jsonify
import redis

application = Flask(__name__)
r = redis.Redis(host='localhost', port=6379, db=0)

@application.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        return jsonify({'routes': ['/insert/<key>/<value>', '/get/<key>', '/remove/<key>',]})
    elif request.method == 'POST':
        return jsonify({'status_code' : 404, 'description' : 'Only GET method is available'})

@application.route('/insert/<key>/<value>', methods=['GET', 'POST'])
def insert(key, value):
    if request.method == 'GET':
        try:
            r.set(key, value)
            return jsonify({'status_code': 200, 'description' : 'Added Successfully'})
        except Exception as err:
            print(err)
            return jsonify({'status_code': 500, 'description' : 'Internal Server Error'})
    elif request.method == 'POST':
        return jsonify({'status_code' : 404, 'description' : 'Only GET method is available'})

@application.route('/get/<key>', methods=['GET', 'POST'])
def get(key):
    if request.method == 'GET':
        try:
            r.get(key)
            return jsonify({'status_code': 200, 'description' : 'Added Successfully'})
        except Exception as err:
            print(err)
            return jsonify({'status_code': 500, 'description' : 'Internal Server Error'})
    
    elif request.method == 'POST':
        return jsonify({'status_code' : 404, 'description' : 'Only GET method is available'})

@application.route('/remove/<key>', methods=['GET', 'POST'])
def remove(key):
    if request.method == 'GET':
        try:
            r.delete(key)
            return jsonify({'status_code': 200, 'description' : 'Added Successfully'})
        except Exception as err:
            print(err)
            return jsonify({'status_code': 500, 'description' : 'Internal Server Error'})
    elif request.method == 'POST':
        return jsonify({'status_code' : 404, 'description' : 'Only GET method is available'})



if __name__ == '__main__':
    application.run(debug=False)